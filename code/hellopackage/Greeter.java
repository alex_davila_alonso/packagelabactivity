package hellopackage;

import java.util.Scanner;
import java.util.Random;
import java.util.*;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String args[]) {

        Utilities uti = new Utilities();

        Scanner input = new Scanner(System.in);
        Random ran = new Random();

        System.out.println("Please add an integer number");
        int num = input.nextInt();

        int newNum = uti.doubleMe(num);
    }
}